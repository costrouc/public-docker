# Public Docker

This is a build repository for open source public packages that I have
not written and there are no official build images. I store all of my
public docker images that fit this condition.


# Images:

 - [lammps](http://lammps.sandia.gov/): [docker image](https://hub.docker.com/r/costrouc/lammps/)
 - [quantum-espresso](http://www.quantum-espresso.org/)
 - [pymatgen](https://github.com/materialsproject/pymatgen/)
 - [polymer-cli](https://www.polymer-project.org/2.0/docs/tools/polymer-cli)
 - [hugo](https://github.com/gohugoio/hugo)

# Official Images

 - [tauitulli](https://github.com/Tautulli/Tautulli)): [official docker image](https://hub.docker.com/r/tautulli/tautulli/)
