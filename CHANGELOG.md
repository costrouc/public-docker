# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a
Changelog](http://keepachangelog.com/en/1.0.0/) and this project
adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

 - plexpy (renamed [tauitulli](https://github.com/Tautulli/Tautulli)) has [official docker image](https://hub.docker.com/r/tautulli/tautulli/)
 - adding [lammps](http://lammps.sandia.gov/)
 - adding [quantum-espresso](http://www.quantum-espresso.org/)
 - adding [pymatgen](https://github.com/materialsproject/pymatgen/)
 - adding [polymer-cli](https://www.polymer-project.org/2.0/docs/tools/polymer-cli)
 - adding [hugo](https://github.com/gohugoio/hugo)
