PACKAGES := lammps
LAMMPS_VERSION := stable_16Mar2018

all: $(addsuffix /build,$(PACKAGES))

lammps/build: lammps/Dockerfile
	docker build -t registry.gitlab.com/costrouc/docker-builds/lammps:$(LAMMPS_VERSION) --build-arg VERSION=$(LAMMPS_VERSION) lammps
	docker push registry.gitlab.com/costrouc/docker-builds/lammps:$(LAMMPS_VERSION)
	touch $@

lammps/run: lammps/build
	docker run -t -i costrouc/lammps:$(LAMMPS_VERSION) /bin/sh
